Basic platformer game to learn the basics of godot.

In this game you can collect coins while jumping and avoiding an enemy.



controls:

Move left with key "Left"
Move right with key "Right"
Jump with key "Up"

What to do?

Collect coins to up that value!

This is licensed by the MIT license.
